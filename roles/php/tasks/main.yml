---
# Variable setup.
- name: Include distribution and version-specific vars.
  include_vars: "{{ item }}"
  with_first_found:
    - files:
        - "{{ ansible_distribution }}-{{ ansible_distribution_major_version }}.yml"
      skip: true

- name: Set the default PHP version for Debian-based OSes.
  set_fact:
    php_version: "{{ __php_version }}"
  when: php_version is not defined and ansible_os_family == 'Debian'

- name: Include OS-specific variables.
  include_vars: "{{ ansible_os_family }}.yml"
  

- name: Remove missing JSON extension for PHP 8.0 (included by default)
  set_fact:
    __php_packages: "{{ __php_packages | reject('search','php' + php_version + '-json') | list }}"
  when:
    - __php_packages is defined
    - php_version is version('8.0', '>=')


- name: Define PHP variables.
  set_fact: "{{ item.key }}={{ lookup('vars', item.value) }}"
  when:
    - vars[item.key] is undefined
    - vars[item.value] is defined
  with_dict:
    php_packages: "{{ __php_packages | list }}"
    php_webserver_daemon: "{{ __php_webserver_daemon }}"
    php_conf_paths: "{{ __php_conf_paths | list }}"
    php_extension_conf_paths: "{{ __php_extension_conf_paths | list }}"
    php_apc_conf_filename: "{{ __php_apc_conf_filename }}"
    php_opcache_conf_filename: "{{ __php_opcache_conf_filename }}"
    php_fpm_daemon: "{{ __php_fpm_daemon }}"
    php_fpm_conf_path: "{{ __php_fpm_conf_path }}"
    php_fpm_pool_conf_path: "{{ __php_fpm_pool_conf_path }}"
    php_mysql_package: "{{ __php_mysql_package }}"
    php_redis_package: "{{ __php_redis_package }}"
    php_memcached_package: "{{ __php_memcached_package }}"
    php_pgsql_package: "{{ __php_pgsql_package }}"
    php_tideways_module_path: "{{ __php_tideways_module_path }}"
    php_uploadprogress_module_path: "{{ __php_uploadprogress_module_path }}"
    php_xdebug_module_path: "{{__php_xdebug_module_path }}"
    php_xhprof_module_path: "{{ __php_xhprof_module_path }}"
    php_fpm_pool_user: "{{ __php_fpm_pool_user }}"
    php_fpm_pool_group: "{{ __php_fpm_pool_group }}"


# Setup/install tasks.

- include_tasks: setup-Debian.yml
  when:
    - not php_install_from_source
    - ansible_os_family == 'Debian'

# Install PHP from source when php_install_from_source is true.
- include_tasks: install-from-source.yml
  when: php_install_from_source

# Configure PHP.
- include_tasks: configure.yml
- include_tasks: configure-apcu.yml
- include_tasks: configure-opcache.yml
- include_tasks: configure-fpm.yml
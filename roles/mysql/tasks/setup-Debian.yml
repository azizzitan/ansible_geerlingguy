---
- name: Check if MySQL is already installed.
  stat: path=/etc/init.d/mysql
  register: mysql_installed

- name: Update apt cache if MySQL is not yet installed.
  apt: update_cache=yes
  when: not mysql_installed.stat.exists

- name: Install mariadb keys
  apt_key:
    keyserver: "{{ mariadb_apt_keyserver }}"
    id: "{{ mariadb_apt_keyid }}"
    state: present

- name: Install mariadb apt repositories
  apt_repository:
    repo: '{{ mariadb_apt_repositories }}'
    state: present
    update_cache: yes
    filename: mariadb

- name: Ensure MySQL Python libraries are installed.
  apt:
    name: "{{ mysql_python_package_debian }}"
    state: present

- name: Ensure MySQL packages are installed.
  apt:
    name: "{{ mysql_packages }}"
    state: present
  register: deb_mysql_install_packages

# Because Ubuntu starts MySQL as part of the install process, we need to stop
# mysql and remove the logfiles in case the user set a custom log file size.
- name: Ensure MySQL is stopped after initial install.
  service: "name={{ mysql_daemon }} state=stopped"
  when: not mysql_installed.stat.exists

- name: Delete innodb log files created by apt package after initial install.
  file: path={{ mysql_datadir }}/{{ item }} state=absent
  with_items:
    - ib_logfile0
    - ib_logfile1
  when: not mysql_installed.stat.exists